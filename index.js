import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../component/splash'
import Home from '../component/home';
import Sampang from '../component/sampang';
import Sumenep from '../component/sumenep';
import Pamekasan from '../component/pamekasan';
import Bangkalan from '../component/bangkalan';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name="splash"
                component={Splash}
                options={{headerShown: false}} />
             <Stack.Screen 
                name="home"
                component={Home}
                options={{headerShown: false}} />
                 <Stack.Screen 
                name="sumenep"
                component={Sumenep}
                />
                 <Stack.Screen 
                name="pamekasan"
                component={Pamekasan}
                 />
                 <Stack.Screen 
                name="sampang"
                component={Sampang}
                 />
                 <Stack.Screen 
                name="bangkalan"
                component={Bangkalan}
                 />    
        </Stack.Navigator>
    );
};

export default Route;
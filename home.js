import React, { Component } from 'react'
import { Text, StyleSheet, View, ImageBackground, TouchableOpacity} from 'react-native'

export default class home extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View style={styles.countainer}>
            <View style={styles.navbar}>
                <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color: 'white',textAlign:'center',marginHorizontal:100}}>WIsata Madura</Text>
            </View>
            <View style={styles.content}>
                <ImageBackground source={require('../assets/pantai.png')} style={{ height: 720,width: 500,}}>
                    <TouchableOpacity style={{borderColor: 'black',width: 150,height:50,paddingHorizontal: 10,borderRadius: 10,marginTop: 15,
                    backgroundColor: 'dodgerblue',color: 'black',justifyContent:"center",alignContent:"center",marginHorizontal:100,marginTop:150}}  onPress={() => this.props.navigation.navigate('sumenep')}>
                        <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color: 'black',textAlign:'center'}}>Sumenep</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{borderColor: 'black',width: 150,height:50,paddingHorizontal: 10,borderRadius: 10,marginTop: 15,
                    backgroundColor: 'dodgerblue',color: 'black',justifyContent:"center",alignContent:"center",marginHorizontal:100,marginTop:50}} onPress={() => this.props.navigation.navigate('pamekasan')}>
                        <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color: 'black',textAlign:'center'}}>Pamekasan</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{borderColor: 'black',width: 150,height:50,paddingHorizontal: 10,borderRadius: 10,marginTop: 15,
                    backgroundColor: 'dodgerblue',color: 'black',justifyContent:"center",alignContent:"center",marginHorizontal:100,marginTop:50}} onPress={() => this.props.navigation.navigate('sampang')}>
                        <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color: 'black',textAlign:'center'}}>Sampang</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{borderColor: 'black',width: 150,height:50,paddingHorizontal: 10,borderRadius: 10,marginTop: 15,
                    backgroundColor: 'dodgerblue',color: 'black',justifyContent:"center",alignContent:"center",marginHorizontal:100,marginTop:50}} onPress={() => this.props.navigation.navigate('bangkalan')}>
                        <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color: 'black',textAlign:'center'}}>Bangkalan</Text>
                    </TouchableOpacity>
        </ImageBackground>
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer: {
        flex: 1,
        backgroundColor: 'white',
    },
    navbar:{
        height: 60,
        backgroundColor: 'black',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    content: {
       
    }
})

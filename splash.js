import React, {useEffect} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('home');
        }, 3000);
    });
    return (
        <View style={Styles.wrapper}>
            <ImageBackground source={require('../assets/splash.jpg')} style={{width:300,height:200}}>
              <Text style={Styles.welcomeText}>Wisata Madura </Text>
            </ImageBackground>
           
        </View>
    );
};

const Styles= StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    welcomeText: {
      fontSize: 30,
      color: 'white',
      fontWeight: 'bold',
      paddingBottom: 20,  
      textAlign:'center',
      marginTop: 10,
    },
});

export default Splash;
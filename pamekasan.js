import React, { Component } from 'react'
import { Text, StyleSheet, View,ScrollView,Image } from 'react-native'

export default class pamekasan extends Component {
    render() {
        return (
            <View style={styles.countainer}>
                <View style={styles.conten}>
                    <ScrollView>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>1. Arek Lancor</Text>
                <Image source={require('../assets/arlan.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>2.Api Takunjung Padam </Text>
                <Image source={require('../assets/api.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>3. Kebun Bunga Matahari</Text>
                <Image source={require('../assets/matahari.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>4. Puncak Ratu</Text>
                <Image source={require('../assets/ratu.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >5. Selamat Pagi Madura </Text>
                <Image source={require('../assets/spm.jpg')} style={{width: 340, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >6. Vihara </Text>
                <Image source={require('../assets/vihara.jpg')} style={{width: 340, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >7. Kapal Jodoh</Text>
                <Image source={require('../assets/kpl.png')} style={{width: 340, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                
                </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer:{
        flex: 1,
        flexDirection: 'column'
    },
    conten:{
        backgroundColor: 'white',
        flex: 1,
    }
})

import React, { Component } from 'react'
import { Text, StyleSheet, View,ScrollView,Image } from 'react-native'

export default class bangkalan extends Component {
    render() {
        return (
            <View style={styles.countainer}>
                <View style={styles.conten}>
                    <ScrollView>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>1. Masjid Agung Bangkalan</Text>
                <Image source={require('../assets/masjidbangkalan.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>2. Bukit Jaddih</Text>
                <Image source={require('../assets/jaddih.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>3. Kapal Rindu</Text>
                <Image source={require('../assets/kplrindu.jpeg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>4. Mersucuar</Text>
                <Image source={require('../assets/marcusuar.png')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >5. Bukit Kapur</Text>
                <Image source={require('../assets/bukitkapur.jpg')} style={{width: 340, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >6. Mangrove Bancaran </Text>
                <Image source={require('../assets/bancaran.jpg')} style={{width: 340, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer:{
        flex: 1,
        flexDirection: 'column'
    },
    conten:{
        backgroundColor: 'white',
        flex: 1,
    }
})

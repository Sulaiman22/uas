import React, { Component } from 'react'
import { Text, StyleSheet, View,ScrollView,Image } from 'react-native'

export default class sumenep extends Component {
    render() {
        return (
            <View style={styles.countainer}>
                <View style={styles.conten}>
                    <ScrollView>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>1. Masjid Agung Sumenep</Text>
                <Image source={require('../assets/masjid.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>2. Asta Tinggi</Text>
                <Image source={require('../assets/asta.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>3. Pantai Gili Labak</Text>
                <Image source={require('../assets/labak.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>4. Pantai Slopeng</Text>
                <Image source={require('../assets/pantai.png')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >5. Pantai Gili Genting</Text>
                <Image source={require('../assets/sembilan.jpg')} style={{width: 340, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >6. Bukit Tinggi </Text>
                <Image source={require('../assets/tinggi.jpg')} style={{width: 340, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >7. Waterr Park Sumekar</Text>
                <Image source={require('../assets/wps.jpg')} style={{width: 340, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                
                </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer:{
        flex: 1,
        flexDirection: 'column'
    },
    conten:{
        backgroundColor: 'white',
        flex: 1,
    }
})

import React, { Component } from 'react'
import { Text, StyleSheet, View,ScrollView,Image } from 'react-native'

export default class sampang extends Component {
    render() {
        return (
            <View style={styles.countainer}>
                <View style={styles.conten}>
                    <ScrollView>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>1. Totampe Beach</Text>
                <Image source={require('../assets/ttb.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>2. Pantai Lon Malang</Text>
                <Image source={require('../assets/lmm.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>3. Pantai Camplong</Text>
                <Image source={require('../assets/camplong.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}}>4. Asela </Text>
                <Image source={require('../assets/asela.jpg')} style={{width: 340, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >5. Monumen Trunojoyo Sampang</Text>
                <Image source={require('../assets/mts.jpg')} style={{width: 340, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:20,color:'black',marginTop:5,marginBottom:5,marginLeft:5}} >6. Air Terjun Toroan </Text>
                <Image source={require('../assets/toroan.jpg')} style={{width: 340, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer:{
        flex: 1,
        flexDirection: 'column'
    },
    conten:{
        backgroundColor: 'white',
        flex: 1,
    }
})
